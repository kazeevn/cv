#!/usr/bin/python
import numpy as np

def mark_10_to_5(x):
    if x in (8,9,10):
        return 5
    elif x in (5,6,7):
        return 4
    elif x in (3,4):
        return 3
    else:
        return None
infile = open("gpa.dat")
tot = 0
tot5 = 0
N = 0

for line in infile:
    line = map(int, line.split(' '))
    tot5 += reduce(lambda x,y: x+y, map(mark_10_to_5, line))
    tot += reduce(lambda x,y: x+y, line)
    N += len(line)

print(float(tot)/N)
print(float(tot5)/N)
